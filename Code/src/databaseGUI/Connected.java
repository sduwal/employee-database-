package databaseGUI;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;

import add_delete.AddCourse;
import add_delete.AddJob;
import add_delete.AddJobCategory;
import add_delete.AddPerson;
import add_delete.DeleteCourse;
import add_delete.DeleteJob;
import add_delete.DeleteJobCategory;
import add_delete.DeletePerson;
import business_processes.BusinessProcesses;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Connected extends JFrame {

	private Connection connection;
	
	public Connected(Connection connection) throws SQLException, FileNotFoundException{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);
		
		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);
		
		JButton questionQueriesButton = new JButton("Question Queries");
		questionQueriesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
						questionQueries(connection);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
		});
		questionQueriesButton.setBounds(15, 26, 199, 43);
		getContentPane().add(questionQueriesButton);
		
		JButton addPerson = new JButton("Add Person");
		addPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					addPerson(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		addPerson.setBounds(15, 78, 199, 53);
		getContentPane().add(addPerson);
		
		JButton deletePerson = new JButton("Delete Person");
		deletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					deletePerson(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		deletePerson.setBounds(229, 78, 199, 53);
		getContentPane().add(deletePerson);
		
		JButton addCourseButton = new JButton("Add Course");
		addCourseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					addCourse(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		addCourseButton.setBounds(15, 135, 199, 53);
		getContentPane().add(addCourseButton);
		
		JButton deleteCourseButton = new JButton("Delete Course");
		deleteCourseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					deleteCourse(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		deleteCourseButton.setBounds(229, 135, 199, 53);
		getContentPane().add(deleteCourseButton);
		
		JButton btnAddJob = new JButton("Add Job");
		btnAddJob.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					addJob(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAddJob.setBounds(15, 193, 199, 53);
		getContentPane().add(btnAddJob);
		
		JButton btnDeleteJob = new JButton("Delete Job");
		btnDeleteJob.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					deleteJob(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnDeleteJob.setBounds(229, 193, 199, 53);
		getContentPane().add(btnDeleteJob);
		
		JButton btnAddJobCategory = new JButton("Add Job Category");
		btnAddJobCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					addJobCategory(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAddJobCategory.setBounds(15, 254, 199, 53);
		getContentPane().add(btnAddJobCategory);
		
		JButton btnDeleteJobCategory = new JButton("Delete Job Category");
		btnDeleteJobCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					deleteJobCategory(connection);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnDeleteJobCategory.setBounds(229, 254, 199, 53);
		getContentPane().add(btnDeleteJobCategory);
		
		JButton btnNewButton = new JButton("Business Processes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					businessProcesses(connection);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		});
		btnNewButton.setBounds(15, 313, 199, 53);
		getContentPane().add(btnNewButton);
		
		JButton btnExecuteIndependentQuery = new JButton("Execute Independent Query");
		btnExecuteIndependentQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					independentQueries(connection);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnExecuteIndependentQuery.setBounds(238, 26, 251, 36);
		getContentPane().add(btnExecuteIndependentQuery);
		
	}

	public void questionQueries(Connection connection) throws FileNotFoundException, SQLException{
			new Queries(connection);
			this.dispose();
	}
	
	public void addPerson(Connection connection) throws FileNotFoundException, SQLException{
		new AddPerson(connection);
		this.dispose();
	}
	
	public void deletePerson(Connection connection) throws FileNotFoundException, SQLException{
		new DeletePerson(connection);
		this.dispose();
	}
	
	public void addCourse(Connection connection) throws FileNotFoundException, SQLException{
		new AddCourse(connection);
		this.dispose();
	}
	
	public void addJob(Connection connection) throws FileNotFoundException, SQLException{
		new AddJob(connection);
		this.dispose();
	}
	
	public void deleteJob(Connection connection) throws FileNotFoundException, SQLException{
		new DeleteJob(connection);
		this.dispose();
	}
	
	public void addCate(Connection connection) throws FileNotFoundException, SQLException{
		new DeleteCourse(connection);
		this.dispose();
	}
	
	public void deleteCate(Connection connection) throws FileNotFoundException, SQLException{
		new DeleteCourse(connection);
		this.dispose();
	}
	
	public void deleteCourse(Connection connection) throws FileNotFoundException, SQLException{
		new DeleteCourse(connection);
		this.dispose();
	}
	
	public void addJobCategory(Connection connection) throws FileNotFoundException,SQLException{
		new AddJobCategory(connection);
		this.dispose();
	}
	
	public void deleteJobCategory(Connection connection) throws FileNotFoundException,SQLException{
		new DeleteJobCategory(connection);
		this.dispose();
	}
	
	public void businessProcesses(Connection connection) throws FileNotFoundException,SQLException{
		new BusinessProcesses(connection);
		this.dispose();
	}
	
	public void independentQueries(Connection connection) throws FileNotFoundException,SQLException{
		new IndependentQueries(connection);
		this.dispose();
	}
}
