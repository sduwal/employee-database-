package databaseGUI;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import javax.swing.JFrame;

public class GUI extends JFrame{

	public static void main(String args[]) throws SQLException,FileNotFoundException{
		LoginGUI login=new LoginGUI();
		
		login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		login.setSize(900, 600);
		login.setResizable(false);
		login.setVisible(true);
	}
}
