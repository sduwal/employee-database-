package databaseGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class GoToHome extends JFrame {

	public GoToHome(Connection connection) throws FileNotFoundException, SQLException {
		new Connected(connection);
		this.dispose();
	}
}
