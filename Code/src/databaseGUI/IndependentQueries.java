package databaseGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IndependentQueries extends JFrame {

	private Connection connection;
	private ResultSet resultSet;
	private JTable table;
	private JTextArea queryArea;

	public IndependentQueries(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);
		
		queryArea = new JTextArea();
		queryArea.setFont(new Font("Courier New", Font.PLAIN, 17));
		queryArea.setText("\r\n");
		queryArea.setBounds(15, 16, 780, 111);
		getContentPane().add(queryArea);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					resultSet=query();
					table = new JTable(buildTableModel(resultSet));
					table.setRowHeight(30);
					table.setRowSelectionAllowed(false);
					table.setColumnSelectionAllowed(false);
					table.setFont(new Font("Tahoma", Font.PLAIN, 20));
					table.setBounds(25, 230, 800, 340);
					table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					getContentPane().add(table);

					//getContentPane().repaint();
					getContentPane().revalidate();	
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnSubmit.setBounds(342, 143, 115, 29);
		getContentPane().add(btnSubmit);

	}
	
	public ResultSet query() throws SQLException{
		String query=queryArea.getText();
		PreparedStatement input=connection.prepareStatement(query);
		return input.executeQuery();
		
	}
	public static DefaultTableModel buildTableModel(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
