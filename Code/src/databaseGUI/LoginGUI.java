package databaseGUI;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;

import connection.DBConnection;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class LoginGUI extends JFrame {
	private JTextField usernameField;
	private JTextField passwordField;
	private JTextField hostField;
	private JTextField sidField;
	private JTextField portField;
	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		getContentPane().setLayout(null);
		this.setResizable(false);
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 775, 492);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUsername.setBounds(10, 25, 112, 52);
		panel.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPassword.setBounds(10, 100, 112, 52);
		panel.add(lblPassword);

		JLabel lblHost = new JLabel("Host");
		lblHost.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHost.setBounds(10, 178, 112, 52);
		panel.add(lblHost);

		JLabel lblNewLabel = new JLabel("SID");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 262, 112, 52);
		panel.add(lblNewLabel);

		JLabel lblPort = new JLabel("Port");
		lblPort.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPort.setBounds(10, 339, 112, 52);
		panel.add(lblPort);

		JButton login = new JButton("LOGIN");
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					login();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		login.setBounds(185, 403, 160, 52);
		panel.add(login);

		usernameField = new JTextField();
		usernameField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		usernameField.setText("spradha1");
		usernameField.setBounds(137, 25, 208, 52);
		panel.add(usernameField);
		usernameField.setColumns(10);

		passwordField = new JTextField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		passwordField.setText("xcXKLV3M");
		passwordField.setBounds(137, 100, 208, 52);
		panel.add(passwordField);
		passwordField.setColumns(10);

		hostField = new JTextField();
		hostField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		hostField.setText("dbsvcs.cs.uno.edu");
		hostField.setBounds(137, 178, 208, 52);
		panel.add(hostField);
		hostField.setColumns(10);

		sidField = new JTextField();
		sidField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		sidField.setText("orcl");
		sidField.setBounds(137, 262, 208, 52);
		panel.add(sidField);
		sidField.setColumns(10);

		portField = new JTextField();
		portField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		portField.setText("1521");
		portField.setBounds(137, 330, 203, 49);
		panel.add(portField);
		portField.setColumns(10);
}

	public void login()throws FileNotFoundException{
		String username = usernameField.getText();
		String password = passwordField.getText();
		String host = hostField.getText();
		String sid = sidField.getText();
		String port = portField.getText();
		
		DBConnection dbConnection=new DBConnection(host,port,sid);
		try {
			Connection connection = dbConnection.getDBConnection(username, password);
			new Connected(connection);
			this.dispose();
		} catch (SQLException a) {
			JOptionPane.showMessageDialog(this, "Connection Failed.", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}
}
