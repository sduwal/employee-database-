package databaseGUI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import connection.QueryDatabase;
import connection.Questions;

import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Queries extends JFrame {

	private Connection connection;
	private JComboBox<String> selectQuestion;
	private String ques;
	private JTextField showQuestion;
	private JTable table;
	private ResultSet resultSet;

	public Queries(Connection connection) throws SQLException, FileNotFoundException {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblQuestionNumber = new JLabel("Question Number");
		lblQuestionNumber.setBounds(15, 28, 200, 50);
		getContentPane().add(lblQuestionNumber);

		showQuestion = new JTextField();
		showQuestion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		showQuestion.setEditable(false);
		showQuestion.setBounds(25, 106, 854, 109);
		getContentPane().add(showQuestion);
		showQuestion.setColumns(10);

		selectQuestion = new JComboBox<String>();
		selectQuestion.setMaximumRowCount(28);
		selectQuestion.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26","27","28","29"}));
		selectQuestion.setSelectedIndex(0);
		selectQuestion.setBounds(230, 34, 48, 39);
		getContentPane().add(selectQuestion);

		JButton btnNewButton = new JButton("SUBMIT");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					ques=showQuestion();
					showQuestion.setText(ques);

					resultSet=query();
					table = new JTable(buildTableModel(resultSet));
					table.setRowHeight(30);
					table.setRowSelectionAllowed(false);
					table.setColumnSelectionAllowed(false);
					table.setFont(new Font("Tahoma", Font.PLAIN, 20));
					table.setBounds(25, 230, 800, 340);
					table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					getContentPane().add(table);

					//getContentPane().repaint();
					getContentPane().revalidate();	
				}catch(Exception e1){
					e1.printStackTrace();
				}

			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 27));
		btnNewButton.setBounds(453, 18, 200, 60);
		getContentPane().add(btnNewButton);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);




	}

	public String showQuestion() throws FileNotFoundException, SQLException{
		String selected=selectQuestion.getSelectedItem().toString();
		int option=Integer.parseInt(selected);

		Questions questions=new Questions();

		String required=questions.getQuestion(option);
		return required;
	}

	public ResultSet query() throws SQLException, FileNotFoundException{
		String selected=selectQuestion.getSelectedItem().toString();
		int option=Integer.parseInt(selected);
		Questions questions=new Questions();

		if(option==1||option==2){
			String input=JOptionPane.showInputDialog("Enter the company ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			resultSet=query.executeQuery();
		}

		else if(option==3||option==23||option==24||option==25||option==26||option==28){
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			resultSet=query.executeQuery();
		}

		else if(option==4||option==5||option==12|option==13||option==14){
			String input=JOptionPane.showInputDialog("Enter the person ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			resultSet=query.executeQuery();
		}

		else if(option==6){
			String input=JOptionPane.showInputDialog("Enter the person ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			query.setInt(2, id);
			resultSet=query.executeQuery();
		}

		else if(option==8||option==9){
			String input=JOptionPane.showInputDialog("Enter the job ID: ");
			String input1=JOptionPane.showInputDialog("Enter the person ID: ");
			int jobid=Integer.parseInt(input);
			int personid=Integer.parseInt(input1);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, jobid);
			query.setInt(2, personid);
			resultSet=query.executeQuery();
		}
		else if(option==10||option==11){
			String input=JOptionPane.showInputDialog("Enter the job ID: ");
			String input1=JOptionPane.showInputDialog("Enter the person ID: ");
			int jobid=Integer.parseInt(input);
			int personid=Integer.parseInt(input1);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, jobid);
			query.setInt(2, personid);
			resultSet=query.executeQuery();
		}

		else if(option==7||option==15||option==22){
			String input=JOptionPane.showInputDialog("Enter the Job ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			resultSet=query.executeQuery();
		}
		else if(option==16||option==18){
			String input=JOptionPane.showInputDialog("Enter the job ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			query.setInt(2, id);
			resultSet=query.executeQuery();
		}
		else if(option==17){
			String input=JOptionPane.showInputDialog("Enter the job ID: ");
			int id=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, id);
			query.setInt(2, id);
			query.setInt(3, id);
			resultSet=query.executeQuery();
		}
		else if(option==19){
			String input=JOptionPane.showInputDialog("Enter the Category code: ");
			String input1=JOptionPane.showInputDialog("Enter the number of misses: ");
			int cate_code=Integer.parseInt(input);
			int k=Integer.parseInt(input1);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, cate_code);
			query.setInt(2, cate_code);
			query.setInt(3, k);
			resultSet=query.executeQuery();
		}
		else if(option==20){
			String input=JOptionPane.showInputDialog("Enter the Category code: ");
			String input1=JOptionPane.showInputDialog("Enter the number of misses: ");
			int cate_code=Integer.parseInt(input);
			int k=Integer.parseInt(input1);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, cate_code);
			query.setInt(2, cate_code);
			query.setInt(3, cate_code);
			query.setInt(4, k);
			query.setInt(5, k);
			resultSet=query.executeQuery();
		}
		else if(option==21||option==27){
			String input=JOptionPane.showInputDialog("Enter the Category Code: ");
			int cate_code=Integer.parseInt(input);
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setInt(1, cate_code);
			resultSet=query.executeQuery();
		}
		
		else if(option==29){
			String sector=JOptionPane.showInputDialog("Enter the Business Sector: ");
			PreparedStatement query=connection.prepareStatement(questions.getQueries(option));
			query.setString(1, sector);
			resultSet=query.executeQuery();
		}

		return resultSet;
	}

	public static DefaultTableModel buildTableModel(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
