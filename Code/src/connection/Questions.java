package connection;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Questions {
	private ArrayList<String> questions;
	private ArrayList<String> queries;

	public Questions() throws FileNotFoundException{
		questions=new ArrayList<String>();
		queries=new ArrayList<String>();
		questionsAdd();
		queriesAdd();
	}
	
	public void questionsAdd() throws FileNotFoundException{
		Scanner scanner=new Scanner(new File("questions.txt"));
		scanner.useDelimiter(";");

		while(scanner.hasNext()){
			questions.add((scanner.next()).trim());
		}
		scanner.close();
	}

	public void queriesAdd() throws FileNotFoundException{
		Scanner scanner = new Scanner(new File("queries.sql"));
		scanner.useDelimiter(";");

		while(scanner.hasNext()){
			queries.add((scanner.next()).trim());
		}
		scanner.close();
	}

	public String getQuestion(int index){
		return questions.get(index-1);
	}

	public int getSize(){
		return questions.size();
	}
	public String getQueries(int index){
		return queries.get(index-1);
	}
}
