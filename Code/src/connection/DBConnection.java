package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	private String dbLocation;
	final String oraThinProtocol="jdbc:oracle:thin";

	public DBConnection(String host,String port,String sID){
		this.dbLocation="@"+host+":"+port+":"+sID;
	}

	public Connection getDBConnection(String username,String password) throws SQLException{
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		// Create the connection
		String url = oraThinProtocol + ':' + dbLocation;
		//System.out.println(url);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

}

