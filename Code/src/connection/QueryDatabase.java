package connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QueryDatabase {

	private Connection connection;
	
	public QueryDatabase(Connection connection) throws SQLException{
		this.connection=connection;
	}
	
	/**
	 * Execute a query passed as string
	 */
	public ResultSet runQuery(String query) throws SQLException{
		PreparedStatement stmt=connection.prepareStatement(query);
		return stmt.executeQuery();
	}
	
	/**
	 * Execute a simple SQL statement like Update 
	 */
	public int runUpdate(String string) throws SQLException{
		PreparedStatement stmt=connection.prepareStatement(string);
		return stmt.executeUpdate();
	}
}
