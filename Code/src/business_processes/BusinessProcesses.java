package business_processes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BusinessProcesses extends JFrame {

	private Connection connection;

	public BusinessProcesses(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("Add New Employee");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					addEmployee(connection);
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(15, 33, 193, 49);
		getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Job Hunting");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					jobHunting(connection);
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(15, 102, 193, 49);
		getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Find right person with training");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					findRightPerson(connection);
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_2.setBounds(15, 167, 247, 59);
		getContentPane().add(btnNewButton_2);

		JButton btnCareerPlanning = new JButton("Career Planning");
		btnCareerPlanning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					careerPlanning(connection);
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnCareerPlanning.setBounds(15, 242, 153, 49);
		getContentPane().add(btnCareerPlanning);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private void jobHunting(Connection connection) throws SQLException {
		new PersonJobHunting(connection);
		this.dispose();
	}

	private void findRightPerson(Connection connection) throws SQLException {
		new FindRightPerson(connection);
		this.dispose();
	}

	private void careerPlanning(Connection connection) throws SQLException {
		new CareerPlanning(connection);
		this.dispose();
	}
	
	private void addEmployee(Connection connection) throws SQLException {
		new AddEmployee(connection);
		this.dispose();
	}

}
