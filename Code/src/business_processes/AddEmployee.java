package business_processes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

public class AddEmployee extends JFrame {
	private Connection connection;
	private JPanel panel;
	private JTextField textField;
	private JTextField textField_1;

	public AddEmployee(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 79, 894, 481);
		panel.setVisible(false);
		getContentPane().add(panel);
		panel.setLayout(null);

		JButton btnAddNewEmployee = new JButton("Add New Employee");
		btnAddNewEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					panel.removeAll();
					addEmployee(connection);
					//panel.revalidate();
					panel.repaint();
					panel.setVisible(true);
				}
				catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnAddNewEmployee.setBounds(15, 20, 182, 45);
		getContentPane().add(btnAddNewEmployee);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();					
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	public void addEmployee(Connection connection){
		JLabel lblPersonId_1 = new JLabel("Person ID");
		lblPersonId_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPersonId_1.setBounds(15, 29, 112, 34);
		panel.add(lblPersonId_1);
		
		JLabel lblJobId = new JLabel("Job ID");
		lblJobId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblJobId.setBounds(15, 79, 79, 34);
		panel.add(lblJobId);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textField.setBounds(141, 29, 176, 31);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textField_1.setColumns(10);
		textField_1.setBounds(141, 84, 176, 31);
		panel.add(textField_1);
		
		JButton btnAddNewEmployee_1 = new JButton("Add New Employee");
		btnAddNewEmployee_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					addNewEmployee();
				}
				catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnAddNewEmployee_1.setBounds(261, 162, 183, 43);
		panel.add(btnAddNewEmployee_1);
	}
	
	public void addNewEmployee() throws SQLException{
		int personID=Integer.parseInt(textField.getText());
		int jobID=Integer.parseInt(textField_1.getText());
		
		String query="update has_job set end_date=trunc(sysdate) where job_id=? and end_date IS null";
		PreparedStatement updateQuery=connection.prepareStatement(query);
		updateQuery.setInt(1, jobID);
		
		String newJob=("insert into has_job (person_id,job_id,start_date,end_date) values (?,?,trunc(sysdate),null)");
		PreparedStatement newJobQuery=connection.prepareStatement(newJob);
		newJobQuery.setInt(1, personID);
		newJobQuery.setInt(2, jobID);
		
		try{
			updateQuery.executeUpdate();
			newJobQuery.executeUpdate();
			final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Person Added.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While adding person", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}finally{
			updateQuery.close();
		}
	}
}




