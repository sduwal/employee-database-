package business_processes;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

public class CareerPlanning extends JFrame {

	private Connection connection;
	private JTextField idField;
	private JTable table;
	private ResultSet resultSet;
	private JTextField missingField;

	public CareerPlanning(Connection connection){
		this.connection=connection;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		try {
			resultSet=findOpportunity();
			table = new JTable(buildTableModel(resultSet));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		table.setRowHeight(30);
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(false);
		table.setFont(new Font("Tahoma", Font.PLAIN, 20));
		table.setBounds(15, 89, 864, 455);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		getContentPane().add(table);
		getContentPane().revalidate();
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private ResultSet findOpportunity() throws SQLException{
		String stringQuery="with vacant_jobs as ((select job_id from job) minus (select job_id from has_job where end_date is null)) select primary_sector, count(job_id) from vacant_jobs natural join job natural join company group by primary_sector";
		PreparedStatement query=connection.prepareStatement(stringQuery);
		return query.executeQuery();
	}

	public static DefaultTableModel buildTableModel(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
