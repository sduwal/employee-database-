package business_processes;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PersonJobHunting extends JFrame {

	private Connection connection;
	private JTextField idField;
	private JTable table;
	private ResultSet resultSet;

	public PersonJobHunting(Connection connection) throws SQLException {
		this.connection=connection;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblPersonId = new JLabel("Person ID");
		lblPersonId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPersonId.setBounds(15, 16, 110, 35);
		getContentPane().add(lblPersonId);

		idField = new JTextField();
		idField.setBounds(150, 16, 196, 35);
		getContentPane().add(idField);
		idField.setColumns(10);
		
		JButton btnNewButton = new JButton("Find Job");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					resultSet=findJob();
					table = new JTable(buildTableModel(resultSet));
					table.setRowHeight(30);
					table.setRowSelectionAllowed(false);
					table.setColumnSelectionAllowed(false);
					table.setFont(new Font("Tahoma", Font.PLAIN, 20));
					table.setBounds(15, 89, 864, 455);
					table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					getContentPane().add(table);
					getContentPane().revalidate();						
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(465, 11, 127, 46);
		getContentPane().add(btnNewButton);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private ResultSet findJob() throws SQLException{
		int id=Integer.parseInt(idField.getText());
		String stringQuery="select J.job_id,J.position from job J where not exists((select k_code from required where job_id = J.job_id) minus (select k_code from has_skill where person_id = ?))";
		PreparedStatement query=connection.prepareStatement(stringQuery);
		query.setInt(1, id);
		return query.executeQuery();
	}
	
	public static DefaultTableModel buildTableModel(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
