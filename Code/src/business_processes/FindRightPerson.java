package business_processes;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

public class FindRightPerson extends JFrame {

	private Connection connection;
	private JTextField idField;
	private JTable table;
	private ResultSet resultSet;
	private JTextField missingField;

	public FindRightPerson(Connection connection){
		this.connection=connection;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblPersonId = new JLabel("Job ID");
		lblPersonId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPersonId.setBounds(15, 16, 110, 35);
		getContentPane().add(lblPersonId);

		idField = new JTextField();
		idField.setBounds(96, 17, 196, 35);
		getContentPane().add(idField);
		idField.setColumns(10);

		JButton btnNewButton = new JButton("Find Person");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					resultSet=findPerson();
					table = new JTable(buildTableModel(resultSet));
					table.setRowHeight(30);
					table.setRowSelectionAllowed(false);
					table.setColumnSelectionAllowed(false);
					table.setFont(new Font("Tahoma", Font.PLAIN, 20));
					table.setBounds(15, 89, 864, 455);
					table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					getContentPane().add(table);
					getContentPane().revalidate();						
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(690, 16, 127, 46);
		getContentPane().add(btnNewButton);
		
		JLabel lblNoOfMissing = new JLabel("No. of Missing Skills");
		lblNoOfMissing.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNoOfMissing.setBounds(307, 20, 165, 31);
		getContentPane().add(lblNoOfMissing);
		
		missingField = new JTextField();
		missingField.setBounds(489, 21, 172, 30);
		getContentPane().add(missingField);
		missingField.setColumns(10);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 66, 115, 29);
		getContentPane().add(btnHome);
	}

	private ResultSet findPerson() throws SQLException{
		int id=Integer.parseInt(idField.getText());
		int number=Integer.parseInt(missingField.getText());
		String stringQuery="with no_of_reqskills(skill_count) as (select count(k_code) from required where job_id = ?), "
				+ "personSkills(person_id, skill_no) as ( select person_id, count(k_code)from has_skill natural join (select k_code from required where job_id = ?)group by person_id)	, "
				+ "missing_skills_list(person_id, no_of_misses) as (select P.person_id, ((select skill_count from no_of_reqskills)- (select skill_no from personSkills PS where PS.person_id = P.person_id)) from person P) select person_id, no_of_misses from missing_skills_list where no_of_misses <= ? order by no_of_misses";
		PreparedStatement query=connection.prepareStatement(stringQuery);
		query.setInt(1, id);
		query.setInt(2, id);
		query.setInt(3, number);
		return query.executeQuery();
	}

	public static DefaultTableModel buildTableModel(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
