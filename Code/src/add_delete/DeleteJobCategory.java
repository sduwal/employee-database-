package add_delete;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class DeleteJobCategory extends JFrame {

	private Connection connection;
	private JTextField idField;

	public DeleteJobCategory(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblPersonId = new JLabel("Category Code");
		lblPersonId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPersonId.setBounds(15, 31, 151, 32);
		getContentPane().add(lblPersonId);

		idField = new JTextField();
		idField.setBounds(218, 29, 146, 39);
		getContentPane().add(idField);
		idField.setColumns(10);

		JButton delete = new JButton("DELETE JOB CATEGORY");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					delete();
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		delete.setBounds(218, 134, 228, 48);
		getContentPane().add(delete);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	public void delete() throws SQLException{
		int cate_code=Integer.parseInt(idField.getText());
		PreparedStatement updateQuery= connection.prepareStatement("delete from job_category where cate_code=?");
		updateQuery.setInt(1, cate_code);
		
		try{
			updateQuery.executeUpdate();
			final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Job Category Deleted.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While deleting Job Category", "Error", JOptionPane.ERROR_MESSAGE);
		}finally{
			updateQuery.close();
		}
		
	}
}
