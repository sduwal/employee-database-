package add_delete;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import connection.QueryDatabase;
import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class AddPerson extends JFrame {

	private Connection connection;
	private JTextField personidField;
	private JTextField nameField;
	private JTextField streetField;
	private JTextField cityField;
	private JTextField stateField;
	private JTextField zipCodeField;
	private JTextField emailField;
	/**
	 * Create the frame.
	 */
	public AddPerson (Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblPersonId = new JLabel("Person ID");
		lblPersonId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPersonId.setBounds(15, 16, 96, 34);
		getContentPane().add(lblPersonId);

		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblName.setBounds(15, 59, 96, 28);
		getContentPane().add(lblName);

		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAddress.setBounds(15, 103, 96, 34);
		getContentPane().add(lblAddress);

		JLabel lblStreet = new JLabel("Street");
		lblStreet.setBounds(61, 151, 69, 20);
		getContentPane().add(lblStreet);

		JLabel lblCity = new JLabel("City");
		lblCity.setBounds(61, 187, 69, 20);
		getContentPane().add(lblCity);

		JLabel lblState = new JLabel("State");
		lblState.setBounds(61, 223, 69, 20);
		getContentPane().add(lblState);

		JLabel lblZipCode = new JLabel("Zip Code");
		lblZipCode.setBounds(61, 259, 69, 20);
		getContentPane().add(lblZipCode);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEmail.setBounds(15, 295, 96, 34);
		getContentPane().add(lblEmail);

		personidField = new JTextField();
		personidField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		personidField.setText("2479434");
		personidField.setBounds(154, 16, 146, 31);
		getContentPane().add(personidField);
		personidField.setColumns(10);

		nameField = new JTextField();
		nameField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nameField.setText("Saroj Duwal");
		nameField.setBounds(154, 57, 306, 34);
		getContentPane().add(nameField);
		nameField.setColumns(10);

		streetField = new JTextField();
		streetField.setText("2000 Lakeshore Dr.");
		streetField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		streetField.setBounds(145, 144, 563, 34);
		getContentPane().add(streetField);
		streetField.setColumns(10);

		cityField = new JTextField();
		cityField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		cityField.setText("New Orleans");
		cityField.setBounds(145, 184, 201, 28);
		getContentPane().add(cityField);
		cityField.setColumns(10);

		stateField = new JTextField();
		stateField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		stateField.setText("LA");
		stateField.setBounds(146, 220, 155, 33);
		getContentPane().add(stateField);
		stateField.setColumns(10);

		zipCodeField = new JTextField();
		zipCodeField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		zipCodeField.setText("70148");
		zipCodeField.setBounds(145, 256, 83, 28);
		getContentPane().add(zipCodeField);
		zipCodeField.setColumns(10);

		emailField = new JTextField();
		emailField.setText("sduwal@uno.edu");
		emailField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		emailField.setBounds(145, 296, 325, 34);
		getContentPane().add(emailField);
		emailField.setColumns(10);

		JButton btnNewButton = new JButton("Add Person");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					addPerson();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(280, 362, 216, 54);
		getContentPane().add(btnNewButton);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	public void addPerson() throws SQLException{
		int personID=Integer.parseInt(personidField.getText());
		String name=nameField.getText();
		String street=streetField.getText();
		String city=cityField.getText();
		String state=stateField.getText();
		int zipCode=Integer.parseInt(zipCodeField.getText()); 
		String email=emailField.getText();

		PreparedStatement updateQuery= connection.prepareStatement("insert into person (person_id,name,street,city,state,zip_code,email) values (?,?,?,?,?,?,?)");
		updateQuery.setInt(1,personID);
		updateQuery.setString(2, name);
		updateQuery.setString(3,street);
		updateQuery.setString(4, city);
		updateQuery.setString(5, state);
		updateQuery.setInt(6, zipCode);
		updateQuery.setString(7, email);
		try{
			updateQuery.executeUpdate();
			 final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Person Added.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While adding person", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}finally{
			updateQuery.close();
		}

	}
}
