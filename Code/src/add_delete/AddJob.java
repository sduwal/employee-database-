package add_delete;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class AddJob extends JFrame {
	private Connection connection;
	private JTextField jobidField;
	private JTextField positionField;
	private JTextField salaryField;
	private JTextField companyidField;
	private JTextField catecodeField;
	private JTextField payTypeField;

	public AddJob(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblCourseId = new JLabel("Job ID");
		lblCourseId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCourseId.setBounds(15, 16, 92, 28);
		getContentPane().add(lblCourseId);

		JLabel lblTitle = new JLabel("Position");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTitle.setBounds(15, 60, 92, 28);
		getContentPane().add(lblTitle);

		JLabel lblCredit = new JLabel("Salary");
		lblCredit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCredit.setBounds(15, 104, 92, 28);
		getContentPane().add(lblCredit);

		JLabel lblRetailPrice = new JLabel("Company ID");
		lblRetailPrice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRetailPrice.setBounds(15, 148, 112, 28);
		getContentPane().add(lblRetailPrice);

		jobidField = new JTextField();
		jobidField.setText("1");
		jobidField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jobidField.setBounds(195, 16, 152, 28);
		getContentPane().add(jobidField);
		jobidField.setColumns(10);

		positionField = new JTextField();
		positionField.setText("Student Worker");
		positionField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		positionField.setColumns(10);
		positionField.setBounds(195, 60, 390, 28);
		getContentPane().add(positionField);

		salaryField = new JTextField();
		salaryField.setText("10.00");
		salaryField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		salaryField.setColumns(10);
		salaryField.setBounds(195, 104, 152, 28);
		getContentPane().add(salaryField);

		companyidField = new JTextField();
		companyidField.setText("300");
		companyidField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		companyidField.setColumns(10);
		companyidField.setBounds(195, 148, 152, 28);
		getContentPane().add(companyidField);

		JButton btnNewButton = new JButton("Add Job");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					addJob();
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(263, 299, 193, 64);
		getContentPane().add(btnNewButton);
		
		JLabel lblCategory = new JLabel("Category Code");
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCategory.setBounds(15, 192, 132, 28);
		getContentPane().add(lblCategory);
		
		JLabel lblPayType = new JLabel("Pay Type");
		lblPayType.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPayType.setBounds(15, 228, 132, 28);
		getContentPane().add(lblPayType);
		
		catecodeField = new JTextField();
		catecodeField.setText("501");
		catecodeField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		catecodeField.setColumns(10);
		catecodeField.setBounds(195, 192, 152, 28);
		getContentPane().add(catecodeField);
		
		payTypeField = new JTextField();
		payTypeField.setText("Wage");
		payTypeField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		payTypeField.setColumns(10);
		payTypeField.setBounds(195, 228, 152, 28);
		getContentPane().add(payTypeField);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private void addJob() throws SQLException{
		int job_id=Integer.parseInt(jobidField.getText());
		String position=positionField.getText();
		float pay_rate=Float.parseFloat(salaryField.getText());
		int comp_id=Integer.parseInt(companyidField.getText());
		int cate_code=Integer.parseInt(catecodeField.getText());
		String pay_type=payTypeField.getText();

		PreparedStatement query=connection.prepareStatement("insert into job (job_id, position, pay_rate, comp_id, cate_code, pay_type) values (?,?,?,?,?,?)");
		
		query.setInt(1,job_id);
		query.setString(2, position);
		query.setFloat(3,pay_rate);
		query.setInt(4, comp_id);
		query.setInt(5, cate_code);
		query.setString(6, pay_type);
		
		
		try{
			query.executeUpdate();
			final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Job Added.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While adding Job", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}finally{
			query.close();
		}
	}
}

