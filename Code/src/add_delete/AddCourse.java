package add_delete;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class AddCourse extends JFrame {

	private Connection connection;
	private JTextField courseidField;
	private JTextField titleField;
	private JTextField creditField;
	private JTextField retailPriceField;

	public AddCourse(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblCourseId = new JLabel("Course ID");
		lblCourseId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCourseId.setBounds(15, 16, 92, 28);
		getContentPane().add(lblCourseId);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTitle.setBounds(15, 60, 92, 28);
		getContentPane().add(lblTitle);

		JLabel lblCredit = new JLabel("Credit");
		lblCredit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCredit.setBounds(15, 104, 92, 28);
		getContentPane().add(lblCredit);

		JLabel lblRetailPrice = new JLabel("Retail Price");
		lblRetailPrice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRetailPrice.setBounds(15, 148, 92, 28);
		getContentPane().add(lblRetailPrice);

		courseidField = new JTextField();
		courseidField.setText("CSCI-100");
		courseidField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		courseidField.setBounds(111, 16, 152, 28);
		getContentPane().add(courseidField);
		courseidField.setColumns(10);

		titleField = new JTextField();
		titleField.setText("Intro to Computer Science");
		titleField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		titleField.setColumns(10);
		titleField.setBounds(111, 62, 390, 28);
		getContentPane().add(titleField);

		creditField = new JTextField();
		creditField.setText("3");
		creditField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		creditField.setColumns(10);
		creditField.setBounds(111, 106, 152, 28);
		getContentPane().add(creditField);

		retailPriceField = new JTextField();
		retailPriceField.setText("100.00");
		retailPriceField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		retailPriceField.setColumns(10);
		retailPriceField.setBounds(111, 150, 152, 28);
		getContentPane().add(retailPriceField);

		JButton btnNewButton = new JButton("Add Course");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					addCourse();
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(287, 257, 193, 64);
		getContentPane().add(btnNewButton);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private void addCourse() throws SQLException{
		String courseID=courseidField.getText();
		String title=titleField.getText();
		int credits=Integer.parseInt(creditField.getText());
		float retailPrice=Float.parseFloat(retailPriceField.getText());

		PreparedStatement query=connection.prepareStatement("insert into course (course_id, title, credits, retail_price) values (?,?,?,?)");
		query.setString(1, courseID);
		query.setString(2, title);
		query.setInt(3, credits);
		query.setFloat(4, retailPrice);
		
		try{
			query.executeUpdate();
			final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Course Added.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While adding course", "Error", JOptionPane.ERROR_MESSAGE);
		}finally{
			query.close();
		}
	}
}

