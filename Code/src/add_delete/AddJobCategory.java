package add_delete;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import connection.QueryDatabase;
import databaseGUI.Connected;
import databaseGUI.GoToHome;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class AddJobCategory extends JFrame {

	private Connection connection;
	private JTextField cateCodeField;
	private JTextField highestField;
	private JTextField lowestField;
	private JTextField titleField;

	public AddJobCategory(Connection connection) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);

		this.connection=connection;
		this.setSize(900, 600);
		getContentPane().setLayout(null);

		JLabel lblCourseId = new JLabel("Category Code");
		lblCourseId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCourseId.setBounds(15, 16, 140, 28);
		getContentPane().add(lblCourseId);

		JLabel lblTitle = new JLabel("Highest Pay");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTitle.setBounds(15, 60, 140, 28);
		getContentPane().add(lblTitle);

		JLabel lblCredit = new JLabel("Lowest Pay");
		lblCredit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCredit.setBounds(15, 104, 152, 28);
		getContentPane().add(lblCredit);

		JLabel lblRetailPrice = new JLabel("Title");
		lblRetailPrice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRetailPrice.setBounds(15, 148, 92, 28);
		getContentPane().add(lblRetailPrice);

		cateCodeField = new JTextField();
		cateCodeField.setText("500");
		cateCodeField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		cateCodeField.setBounds(237, 16, 152, 28);
		getContentPane().add(cateCodeField);
		cateCodeField.setColumns(10);

		highestField = new JTextField();
		highestField.setText("20.00");
		highestField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		highestField.setColumns(10);
		highestField.setBounds(237, 60, 152, 28);
		getContentPane().add(highestField);

		lowestField = new JTextField();
		lowestField.setText("7.25");
		lowestField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lowestField.setColumns(10);
		lowestField.setBounds(237, 104, 152, 28);
		getContentPane().add(lowestField);

		titleField = new JTextField();
		titleField.setText("Student Worker");
		titleField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		titleField.setColumns(10);
		titleField.setBounds(237, 148, 375, 28);
		getContentPane().add(titleField);

		JButton addButton = new JButton("Add Job Category");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					addJobCategory();
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		addButton.setBounds(285, 226, 193, 64);
		getContentPane().add(addButton);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					new GoToHome(connection);
					dispose();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnHome.setBounds(764, 28, 115, 29);
		getContentPane().add(btnHome);
	}

	private void addJobCategory() throws SQLException{
		int cate_code=Integer.parseInt(cateCodeField.getText());
		float highest=Float.parseFloat(highestField.getText());
		float lowest=Float.parseFloat(lowestField.getText());
		String title=titleField.getText();

		PreparedStatement query=connection.prepareStatement("insert into job_category (cate_code, pay_range_high, pay_range_low, title) values (?,?,?,?)");
		
		query.setInt(1, cate_code);
		query.setFloat(2,highest);
		query.setFloat(3, lowest);
		query.setString(4, title);
		
		try{
			query.executeUpdate();
			final ImageIcon icon = new ImageIcon(new URL("file:///D:/Eclipse/Database/src/databaseGUI/tick-512.png"));
			JOptionPane.showMessageDialog(this, "Job Category Added.", "Success", JOptionPane.OK_OPTION,icon);
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Error While adding Job Category", "Error", JOptionPane.ERROR_MESSAGE);
		}finally{
			query.close();
		}
	}
}

