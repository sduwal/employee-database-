select name, comp_id
    from person natural join has_job natural join job
	where comp_id = ? 
	and end_date is null
	order by name;

  select name, pay_rate, comp_id
	from person natural join has_job natural join job
	where comp_id = ?
	and pay_type = 'Staff'
	and end_date is null
	order by pay_rate desc;

  with salary(comp_id, salary_amount) as (
		select comp_id, sum(pay_rate)
		from job natural join has_job
		where pay_type = 'Staff'
		and end_date is null
		group by comp_id)
	, wage(comp_id, salary_amount) as (
		select comp_id, sum(pay_rate*1920)
		from job natural join has_job
		where pay_type = 'Wage'
		and end_date is null
		group by comp_id)
	, total(comp_id, salary_amount) as (
		(select comp_id, salary_amount from salary)
		union
		(select comp_id, salary_amount from wage))
	select comp_id, sum(salary_amount) as labor_cost
	from total 
	group by comp_id
	order by labor_cost desc;

 select job_id, position
	from has_job natural join job
	where person_id = ?;

  select title, k_code
	from has_skill natural join knowledge_skill
	where person_id = ?;

  (select k_code
	from has_job natural join required
	where person_id = ?) 
	minus
	(select k_code
	from has_skill
	where person_id = ?);
	
  select title, k_code
	from required natural join knowledge_skill
	where job_id = ?;

 	(select k_code
	from required
	where job_id = ?)
	minus
	(select k_code
	from has_skill
	where person_id = ?);

  with missing(skills) as (
			(select k_code
			from required
			where job_id = ?)
			minus
			(select k_code
			from has_skill
			where person_id = ?))
	select course_id, title
	from course C
	where not exists(
		(select skills
		from missing)
		minus
		(select k_code
		from course_skill CS
		where CS.course_id = C.course_id));

with missing(skills) as (
			(select k_code
			from required
			where job_id = ?)
			minus
			(select k_code
			from has_skill
			where person_id = ?))
	, sections_needed(course, sec, completion_date) as (
		select S.course_id, S.sec_id, S.complete_date 
		from section S
		where not exists(
			(select skills
			from missing)
			minus
			(select k_code
			from section natural join course natural join course_skill
			where course_id = S.course_id and sec_id = S.sec_id)))
	select *
	from sections_needed
	where completion_date <= (select min(completion_date) from sections_needed)
	and completion_date>trunc(sysdate);

 with missing(skills) as (
			(select k_code
			from required
			where job_id = ?)
			minus
			(select k_code
			from person natural join has_skill
			where person_id = ?))
	,sections_needed(course, sec, cost, completion_date) as (
		select S.course_id, S.sec_id, S.price, S.complete_date 
		from section S
		where not exists(
			(select skills
			from missing)
			minus
			(select k_code
			from section natural join course natural join course_skill
			where course_id = S.course_id and sec_id = S.sec_id)))
	select T.course, T.sec, T.cost
	from sections_needed T
	where T.cost = (select min(cost) from sections_needed)
	and completion_date>trunc(sysdate);

with missed_skills as (
		(select k_code
		from skills)
		minus
		(select k_code
		from has_skill
		where person_id = ?))
	, Setskills(set_id, k_code) as (
		select set_id, k_code
		from CourseSet S join course_skill CS on S.c1 = CS.course_id
		union
		select set_id, k_code
		from CourseSet S join course_skill CS on S.c2 = CS.course_id
		union
		select set_id, k_code
		from CourseSet S join course_skill CS on S.c3 = CS.course_id)
	, CoverSet(set_id, c1, c2, c3) as (
		select S.set_id, S.c1, S.c2, S.c3
		from CourseSet S
		where not exists(
			(select k_code
			from missed_skills)
			minus
			(select k_code
			from Setskills SS
			where SS.set_id = S.set_id)))
	, set_cost(set_id, c1, c2, c3, cost) as (
		select distinct S.set_id, S.c1, S.c2, S.c3, sum(C.retail_price)
		from CoverSet S, course C
		where C.course_id = S.c1 or C.course_id = S.c2 or C.course_id = S.c3
		group by (set_id, c1, c2, c3))
	select set_id, c1, c2, c3, cost
	from set_cost
	order by cost; 
	
 select J.cate_code
	from job_category J
	where not exists(
		(select k_code
		from core
		where cate_code = J.cate_code)
		minus
		(select k_code
		from has_skill
	    where person_id = ?));
	
with payment(job_id, position, pay_rate,pay_type) as (
		select J.job_id, J.position, pay_rate,pay_type
		from job J
		where not exists(
			(select k_code
			from required
			where job_id = J.job_id)
			minus
			(select k_code
			from has_skill
			where person_id = ?)))
	,pays(job_id,position,pay_rate) as(
		(select job_id,position,pay_rate
			from payment
			where pay_type='Staff')
		union
		(select job_id,position,pay_rate*1920
		from payment
		where pay_type='Wage'))
	select P.job_id, P.position
	from pays P
	where P.pay_rate = (select max(pay_rate) from pays);

 select P.name, P.email
	from person P
	where not exists(
		(select k_code
		from required
		where job_id = ?)
		minus
		(select k_code
		from has_skill
		where person_id = P.person_id));

 with no_of_reqskills(skill_count) as (
		select count(k_code)
		from required
		where job_id = ?)
    , personSkills(person_id, skill_no) as (
		select person_id, count(k_code)
		from has_skill natural join (select k_code from required where job_id = ?)
		group by person_id)		
	select P.person_id, P.name
	from person P
	where 1 = (
		(select skill_count
		from no_of_reqskills)
		-
		(select skill_no 
		from personSkills PS
		where PS.person_id = P.person_id));
		
 with no_of_reqskills(skill_count) as (
		select count(k_code)
		from required
		where job_id = ?)
    , personSkills(person_id, skill_no) as (
		select person_id, count(k_code)
		from has_skill natural join (select k_code from required where job_id = ?) 
		group by person_id)		
	, missing_one_list(person_id) as (
		select P.person_id
		from person P
		where 1 = (
			(select skill_count
			from no_of_reqskills)
			-
			(select skill_no 
			from personSkills PS
			where PS.person_id = P.person_id)))
	, missed_skill(person_id, k_code) as (
		(select person_id, k_code
		from missing_one_list, required 
		where job_id = ?)
		minus
		(select person_id, k_code 
		from missing_one_list natural join has_skill))
	select k_code, count(person_id) as no_of_people_missing
	from missed_skill
	group by k_code
	order by count(person_id);
	

 with person_and_misses as(
		select person_id, (
			(select count(k_code) 
			from required
			where job_id = ?) 
			-
			(select count(k_code) 
			from has_skill H
			where P.person_id = H.person_id and k_code in (
												select k_code 
												from required
												where job_id = ?))) as no_of_missed_skills
		from person P
		group by person_id)
	select person_id, no_of_missed_skills
	from person_and_misses
	where no_of_missed_skills = (select min(no_of_missed_skills) from person_and_misses); 
	

 with no_of_reqskills(skill_count) as (
		select count(k_code)
		from core
		where cate_code = ?)
    , personSkills(person_id, skill_no) as (
		select person_id, count(k_code)
		from has_skill
		where k_code in (select k_code from core where cate_code = ?)
		group by person_id)		
	, missing_skills_list(person_id, no_of_misses) as (
		select P.person_id, (
			(select skill_count
			from no_of_reqskills)
			-
			(select skill_no 
			from personSkills PS
			where PS.person_id = P.person_id))
		from person P)
	select person_id, no_of_misses
	from missing_skills_list
	where no_of_misses <= ?
	order by no_of_misses;
	

with no_of_reqskills(skill_count) as (
		select count(k_code)
		from core
		where cate_code = ?)
    , personSkills(person_id, skill_no) as (
		select person_id, count(k_code)
		from has_skill natural join (select k_code from core where cate_code = ?)
		group by person_id)		
	, missing_k_list(person_id, k) as (
		select P.person_id, (
			(select skill_count
			from no_of_reqskills)
			-
			(select skill_no 
			from personSkills PS
			where PS.person_id = P.person_id))
		from person P)
	, missed_core(person_id, k_code) as (
		(select person_id, k_code
		from missing_k_list, core 
		where cate_code = ?
		and k <= ?)
		minus
		(select person_id, k_code 
		from missing_k_list natural join has_skill
		where k <= ?))
	select k_code, count(person_id) as people_without_core
	from missed_core
	group by k_code
	order by count(person_id) desc;

 select person_id
	from has_job natural join job natural join job_category
	where cate_code = ?;

 with unemployed as(
		(select person_id
		from person)
		minus 
		(select person_id
		from has_job
		where end_date IS NULL
		or end_date>trunc(sysdate)))
	select person_id
	from unemployed natural join has_job
	where job_id = ?;

	with emp(comp_id, employees) as (
		select comp_id, count(person_id)
		from has_job natural join job
		where end_date is null
		group by comp_id)
	select comp_id, c_name
	from emp E natural join company
	where E.employees = (select max(employees) from emp);
	
 with emp(primary_sector, employees) as (
		select primary_sector, count(person_id)
		from has_job natural join job natural join company
		where end_date is null
		group by primary_sector)
	select E.primary_sector
	from emp E
	where E.employees = (select max(employees) from emp);

with present_jobs(person_id, latest, pay_rate) as (
		select person_id, start_date, pay_rate
		from has_job natural join job
		where end_date is null)
	, second_last_job(person_id, pay_rate) as (
		select H.person_id, J.pay_rate
		from has_job H join job J using (job_id)
		where H.start_date = (
			select max(start_date)
			from has_job HJ
			where HJ.start_date < (select latest from present_jobs where person_id = HJ.person_id)
			and HJ.person_id = H.person_id))
	, increase(no) as (
		select count(P.person_id)
		from present_jobs P, second_last_job S
		where P.pay_rate > S.pay_rate
		and P.person_id = S.person_id)
	, decrease(no) as (
		select count(P.person_id)
		from present_jobs P, second_last_job S
		where P.pay_rate < S.pay_rate
		and P.person_id = S.person_id)
	select I.no/D.no
	from increase I, decrease D;

 with vacant_jobs as (
		(select job_id
		from job)
		minus
		(select job_id
		from has_job
		where end_date is null))
	, vacancy(cate_code, vacancies) as (
		(select cate_code, count(job_id)
		from vacant_jobs natural join job
		group by cate_code))
	, qualified(cate_code, person_id) as (
		select cate_code, person_id
		from vacancy V, person P
		where not exists(
			(select k_code
			from core C
			where C.cate_code = V.cate_code)
			minus
			(select k_code
			from has_skill H
			where H.person_id = P.person_id)))
	, num_of_qualified(cate_code, people) as (
		select cate_code, count(person_id)
		from qualified
		group by cate_code)
	, diff(cate_code, difference) as (
		select cate_code, (vacancies - people)
		from vacancy natural join num_of_qualified)
	select cate_code
	from diff
	where difference = (select max(difference) from diff);

	select title, k_code
	from core natural join knowledge_skill
	where cate_code = ?;
	
	select primary_sector, count(job_id)
	from job natural join company
	group by primary_sector;
	
	with present_jobs(person_id, latest, pay_rate, job_id) as (
		select person_id, start_date, pay_rate, job_id
		from has_job H join job J using (job_id)
		where start_date = (select max(start_date) from has_job where person_id = H.person_id))
	, present_sector(person_id, latest, pay_rate) as (
		select person_id, latest, pay_rate
		from present_jobs natural join job natural join company
		where primary_sector = ?)
	, second_last_job(person_id, pay_rate) as (
		select H.person_id, J.pay_rate
		from has_job H join job J using (job_id)
		where H.start_date = (
			select max(start_date)
			from has_job HJ
			where HJ.start_date < (select latest from present_sector where person_id = HJ.person_id)
			and HJ.person_id = H.person_id))
	, increase(person_id, imp) as (
		select P.person_id, (P.pay_rate - S.pay_rate) 
		from present_jobs P, second_last_job S
		where P.pay_rate > S.pay_rate
		and P.person_id = S.person_id)
	select sum(imp)/count(person_id)
	from increase;
